# Token Generator
This cli can generate random string of 7 chars to a text file.
Then read these tokens in batches to store them in `postgres`
while handling the already exising tokens with `On Conflict` feature in `postgres`
https://www.postgresql.org/docs/current/sql-insert.html#SQL-ON-CONFLICT

### Usage
The `docker-compose.yml` file has `postgres` and `gomigrate` images to start up the env
```shell
docker-compose up
```

To use the generator and the reader we can execute the following
```shell
# generate tokens
go run *.go generate --count=10000000

# read tokens from file and insert into posgresql
go run *.go reader --batch=512 --db=<connection-string>

# list duplicates
go run *.go duplicates --db=<connection-string>
```

### Database Scheme

```sql
CREATE TABLE TOKENS
(
    ID         SERIAL      PRIMARY KEY,
    TOKEN      VARCHAR(7)  NOT NULL UNIQUE,
    FREQUENCY  NUMERIC     NOT NULL DEFAULT 1
);
```
