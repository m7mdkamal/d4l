package main

import (
	"database/sql"
	"fmt"
	"log"
	"strings"
)

type database struct {
	db *sql.DB
}

func NewDatabaseConnection(connStr string) *database {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	return &database{db: db}
}

// save the tokens into the database by using postgres `INSERT ON CONFLICT` feature
// https://www.postgresql.org/docs/current/sql-insert.html#SQL-ON-CONFLICT
func (d *database) saveIntoDB(tokens []string) {

	// using map to count the duplicated in the same batch
	// because we can't update the same token twice in the same sql statement
	var mp = make(map[string]int)
	for _, token := range tokens {
		if value, exists := mp[token]; !exists {
			mp[token] = 1

		} else {
			mp[token] = value + 1
		}
	}

	var sqlValues []string
	var sqlArgs []interface{}
	var i = 0
	for key, value := range mp {
		sqlValues = append(sqlValues, fmt.Sprintf("($%d, $%d)", (i*2)+1, (i*2)+2))
		sqlArgs = append(sqlArgs, key, value)
		i += 1
	}

	_, err := d.db.Exec(`
	INSERT INTO TOKENS AS T (TOKEN, FREQUENCY)
	VALUES `+strings.Join(sqlValues, ",")+`
	ON CONFLICT (TOKEN)
	DO UPDATE SET FREQUENCY = T.FREQUENCY + EXCLUDED.FREQUENCY
	`, sqlArgs...)

	if err != nil {
		log.Fatal(err)
	}
}

// printDuplicates for printing non unique tokens
func (d *database) printDuplicates() {

	// we can speed up this query by adding index on FREQUENCY col but this will slow down the insertion
	rows, err := d.db.Query(`
		SELECT TOKEN, FREQUENCY
		FROM TOKENS
		WHERE FREQUENCY > 1
		ORDER BY FREQUENCY DESC
`)

	if err != nil {
		panic(err)
	}
	defer rows.Close()

	type token struct {
		Token     string
		Frequency int
	}

	var tokens []token

	for rows.Next() {
		var t token
		err = rows.Scan(&t.Token, &t.Frequency)
		if err != nil {
			log.Fatal(err)
		}
		tokens = append(tokens, t)
	}

	log.Printf("found %d non unique tokens\n", len(tokens))
	log.Println(tokens)
}
