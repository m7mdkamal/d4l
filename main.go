package main

import (
	"flag"
	_ "github.com/lib/pq"
	"log"
	"os"
)

func main() {
	generateCommand := flag.NewFlagSet("generate", flag.ExitOnError)
	readerCommand := flag.NewFlagSet("reader", flag.ExitOnError)
	duplicatesCommand := flag.NewFlagSet("duplicates", flag.ExitOnError)

	numberOfTokens := generateCommand.Int("count", 10_000_000, "number of tokens")
	tokenLength := generateCommand.Int("length", 7, "token length")
	outputFile := generateCommand.String("output", "tokens.txt", "output file")

	dbConnection := readerCommand.String("db", "postgresql://postgres:postgres@192.168.56.101:5432/postgres?sslmode=disable", "pg connection string")
	batchSize := readerCommand.Int("batch", 100, "batch size")
	inputFile := readerCommand.String("input", "tokens.txt", "input file")

	duplicatesDbConnection := duplicatesCommand.String("db", "postgresql://postgres:postgres@192.168.56.101:5432/postgres?sslmode=disable", "pg connection string")

	if len(os.Args) < 2 {
		log.Println("[error] generate or reader subcommand is required")
		os.Exit(1)
	}

	switch os.Args[1] {
	case "generate":
		generateCommand.Parse(os.Args[2:])
	case "reader":
		readerCommand.Parse(os.Args[2:])
	case "duplicates":
		duplicatesCommand.Parse(os.Args[2:])
	default:
		flag.PrintDefaults()
		os.Exit(1)
	}

	if generateCommand.Parsed() {
		log.Printf(
			"[debug] creating %d tokens of length: %d, to output file: `%s`\n",
			*numberOfTokens,
			*tokenLength,
			*outputFile,
		)
		handleGenerateCommand(*outputFile, *tokenLength, *numberOfTokens)
	}

	if readerCommand.Parsed() {
		log.Printf(
			"[debug] reading file: `%s` with batch size: %d\n",
			*inputFile,
			*batchSize,
		)
		handleReaderCommand(*dbConnection, *inputFile, *batchSize)
	}

	if duplicatesCommand.Parsed() {
		log.Printf("[debug] counting non unique token for the database\n")
		handleDuplicatesCommand(*duplicatesDbConnection)
	}
}

func handleGenerateCommand(outputFile string, tokenLength, numberOfTokens int) {
	var generator = stringGenerator{tokenLength}

	fo, err := os.Create(outputFile)
	if err != nil {
		log.Printf("[error] while reading the output file: %s\n", err.Error())
		return
	}
	defer fo.Close()

	generator.generate(numberOfTokens, fo)
}

func handleReaderCommand(dbStr string, inputFile string, batchSize int) {

	database := NewDatabaseConnection(dbStr)

	fi, err := os.Open(inputFile)
	if err != nil {
		log.Printf("[error] while reading the input file: %s\n", err.Error())
		return
	}
	defer fi.Close()

	tokenScanner := scan(fi, batchSize)
	for {
		hasNext, tokens := tokenScanner()

		if len(tokens) > 0 {
			database.saveIntoDB(tokens)
		}

		if !hasNext {
			break
		}
	}
}

func handleDuplicatesCommand(dbStr string) {
	database := NewDatabaseConnection(dbStr)
	database.printDuplicates()
}
