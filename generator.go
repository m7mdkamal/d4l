package main

import (
	"io"
	"math/rand"
	"time"
)

type generator interface {
	generate(n int, writer io.Writer)
}

type stringGenerator struct {
	tokenLength int
}

// generate for generating random n char from a-z letters
func (g *stringGenerator) generate(numberOfTokens int, writer io.Writer) {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyz")
	rand.Seed(time.Now().UnixNano())
	for numberOfTokens != 0 {
		b := make([]rune, g.tokenLength)
		for i := range b {
			b[i] = letterRunes[rand.Intn(len(letterRunes))]
		}

		writer.Write([]byte(string(b) + "\n"))
		numberOfTokens -= 1
	}
}
