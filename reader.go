package main

import (
	"bufio"
	"io"
	"log"
)

// scan for reading the input file line by line in batches
func scan(inputFile io.Reader, batchSize int) func() (bool, []string) {

	scanner := bufio.NewScanner(inputFile)

	var maxCapacity int = 1_024
	buf := make([]byte, maxCapacity)
	scanner.Buffer(buf, maxCapacity)
	scanner.Split(bufio.ScanLines)

	var totalTokens = 0

	return func() (bool, []string) {
		var scannedTokens = []string{}
		for scanner.Scan() {
			scannedTokens = append(scannedTokens, scanner.Text())
			if len(scannedTokens) == batchSize {
				totalTokens += len(scannedTokens)
				log.Printf("[debug] numberOfTokens=%d totalTokens=%d\n", batchSize, totalTokens)
				return true, scannedTokens
			}
		}
		totalTokens += len(scannedTokens)
		log.Printf("[debug] numberOfTokens=%d totalTokens=%d\n", len(scannedTokens), totalTokens)
		return false, scannedTokens
	}
}
